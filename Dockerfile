FROM node:12.21.0

WORKDIR /app
COPY . /app
RUN npm install

ENTRYPOINT ["npm", "start"]