# SIMPLE UPLOAD

### Tecnologys
- Nodejs
- Express

### Description

US: This project is aimed at people who need something easy and functional to make 
files download available.

PT-BR: Este projeto é destinado as pessoas que precisam de algo fácil e funcional
para disponibilizar downloads de arquivos.


### ENVIROMENT
DIR_FILES


### EXAMPLE
````
#git clone https://gitlab.com/giobalarini/downloadfile.git
#docker image build -t gov/easydownload .
#docker container run -dti --name easydownload -v /download/:/download -e DIR_FILES=/download -p4000:4000 gov/easydownload
````
