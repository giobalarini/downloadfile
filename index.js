const http = require('http');
const express = require('express');
const app = express();
const fs = require('fs');
const directory = process.env.DIR_FILES || `${__dirname}/upload-folder/`;



app.get('/download', function(req, res){
    fs.readdir(directory, (err, files) => {
        res.status(200).json({files: files});
    });
});

app.get('/download/:file_name', function(req, res){
    console.log(req.params.file_name)
    const file = directory+"/"+req.params.file_name;
    res.download(file); // Set disposition and send it.
});

const server = http.createServer(app);
server.listen(4000);
console.log("Starting server at port 4000")
console.log("Folder Selected: "+directory)